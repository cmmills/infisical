#! /usr/bin/env python3

"""
Set up infisical server
"""

import os

def build():
    """
    Update and upgrade system, install docker-compose, and run docker-compose up --build
    to install infisical server
    """
    os.system("apt-get update -y")
    os.system("apt-get upgrade -y")
    os.system("apt install docker-compose -y")
    os.system("docker-compose up --build")

    url1 = "https://raw.githubusercontent.com/Infisical/infisical/main/.env.example"
    url2 = "https://raw.githubusercontent.com/Infisical/infisical/main/docker-compose.yml"
    url3 = "https://raw.githubusercontent.com/Infisical/infisical/main/nginx/default.dev.conf"

    os.mkdir("infisical")
    os.chdir("infisical")
    os.system("wget " + url1)
    os.rename(".env.example", ".env")
    os.system("wget " + url2)
    os.mkdir("nginx")
    os.chdir("nginx")
    os.system("wget " + url3)
    os.rename("default.dev.conf", "default.conf")
    os.chdir("..")
    os.system("docker-compose up --build")


if __name__ == "__main__":
    build()