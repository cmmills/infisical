# Infisical

Open Source Secret [OPS](http://infisical.com) - OPEN SOURCE,END-TO-END ENCRYPTED platform that lets you securely manage secrets and configs across your team, devices, and infrastructure

## Installation

### Infisical server
This is the managment portal for managing secrets withing environments.
-  The [build.py](build.py) script uses the following [docker-compose](https://infisical.com/docs/self-hosting/deployment-options/docker-compose) instructions to build a working infisical server, tested on Digital ocean using the following [automated build process](https://gitlab.com/cmmills/python/-/tree/master/digital_ocean)
- HowTO - doucmentation coming soon

### Infisical CLI
- [CLI](https://infisical.com/docs/cli/overview)
- HowTO -documentation coming soon